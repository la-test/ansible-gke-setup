# ansible-gke-setup

Репозиторий содержит набор Ansible-плейбуков с ролью **setup-gke-cluster**, предназначенной для создания кластера GKE.

Основная поддержка GKE в Ansible реализована в модулях [gcp_container_cluster](https://docs.ansible.com/ansible/2.6/modules/gcp_container_cluster_module.html) и [gcp_container_node_pool](https://docs.ansible.com/ansible/2.6/modules/gcp_container_node_pool_module.html), анонсированных в версии Ansible 2.6.

## Замечания

Предварительное тестирование показало неудовлетворительную работу данных модулей:

 - нет возможности указать некоторые из параметров кластера
 - отсутствует возможность создания кластера без пула нод по-умолчанию, что вызывает необходимость удаления этого пула отдельным таском для Ansible
 - [проблемы](https://github.com/GoogleCloudPlatform/magic-modules/issues/394) с сохранением идемпотентности (при изменении какого-либо параметра, Ansible считает, что изменений в конфигурации кластера проводить не требуется). Удаления пула нод по-умолчанию при подобной работе Ansible может быть крайне рискованной операцией, например, в том случае, если данный пул перед применением плейбука был создан ранее вручную, и на нем выполняется полезная нагрузка.

Использование Ansible ветки 2.7-dev0 также не принесло положительных результатов.
Перечисленные выше факты не позволяет рекомендовать применение данного инструмента для развертывания GKE в промышленной среде.
В качестве альтернативного способа создания GKE можно рассмотреть другие инструменты с более зрелой поддержкой GCP и GKE, например, **Terraform**.

## Структура репозитория

    ├── environments
    │   ├── prod-cluster
    │   │   └── group_vars
    │   │       └── all
    │   └── test-cluster
    │       └── group_vars
    │           └── all
    ├── playbook.yml
    └── roles
        └── setup-gke-cluster
            ├── defaults
            │   └── main.yml
            ├── files
            │
            ├── handlers
            │   └── main.yml
            ├── meta
            │   └── main.yml
            ├── tasks
            │   ├── create_node_pool.yml
            │   ├── delete_default_node_pool.yml
            │   ├── main.yml
            │   └── setup_gke.yml
            ├── templates
            ├── tests
            │   ├── inventory
            │   └── test.yml
            └── vars
                └── main.yml
                
### Переменные:
`roles/setup-gke-cluster/defaults/main.yml` - переменные по-умолчанию

`environments/{environment_name}/group_var/all` - переменные для каждого из окружений `environment_name`

### Ansible Tasks
Выполнение плейбука производится в следующем порядке:
1. Проверка окружения (для работы требуется Ansible >= 2.6)
2. Создание кластера GKE с пулом нод по-умолчанию, состоящим из одной ноды (`setup_gke.yml`)
3. Создание основного пула нод с заданными характеристиками (`create_node_pool.yml`)
4. Удаление пула нод по-умолчанию (`delete_default_node_pool.yml`)

## Пример запуска 
`ansible-playbook -i environments/test-cluster playbook.yml`

## Pipeline
Для тестирования процесса создания кластера в GCP создан отдельный проект (в данном случае, `test-task-213422`). Промышленной среде соответствует проект `prod-task`.
В каждом из проектов создан service account с перечисленными ниже ролями, от лица которого выполняются все задачи данного плейбука:

 - Compute Admin
 - Kubernetes Engine Admin
 - Service Account User
 
Ключи сервисных акаунтов передаются раннеру через переменные окружения в проекте ansible-gke-setup GitLab (в Ansible еще нет поддержки Google KMS):

- `CI_GCP_PROD_ENV_SERVICE_KEY` (для промышленной среды, присутствует только при сборке на защищенных ветках, например, master )
- `CI_GCP_TEST_ENV_SERVICE_KEY` (для тестового проекта) 

Для чтения docker registry проекта [ansible-gauth-containter](https://gitlab.com/la-test/ansible-gauth-containter) используются переменные 

- `ANSIBLE_GAUTH_CONTAINER_PROJECT_NAME`
- `ANSIBLE_GAUTH_CONTAINER_REGISTRY_PASS`
- `ANSIBLE_GAUTH_CONTAINER_REGISTRY_USER`

### Стадии pipeline
1. **Check.** Проверка синтаксиса плейбука
2. **Deploy_test_infra.** Создание кластера GKE в отдельном тестовом проекте CGP `test-task-213422`
3. **Deploy_prod_infra** (запуск выполняется вручную и только на ветке master). Создание кластера в основном проекте GCP `prod-task`.

## TODO
1. Тестирование состояния кластера после его создания (например, при помощи testinfra или molecule)
2. Деплой приложения в тестовый проект с последующей минимальной проверкой
3. Создание кастомных сетей для нод (на данный момент используется сеть `default`)
4. Поддержка создания произвольного количества пулов нод
5. Предусмотреть возможность удаления кластера в тестовом проекте
6. Отменить автоматическое удаление пула нод по-умолчанию (default-pool) в кластере промышленной среды
